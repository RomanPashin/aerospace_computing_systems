# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

import cv2
import requests

# Путь к файлу
file_path = "tests/fixtures/fixtures/file_19.jpg"

# Открытие изображения с помощью OpenCV и преобразование в массив байтов
img = cv2.imread(file_path)
ret, img_bytes = cv2.imencode(".jpg", img)
img_bytes = img_bytes.tobytes()

# Отправка запроса на сервер
url = "http://0.0.0.0:8080/predict"
response = requests.post(url, data=img_bytes)

# Вывод ответа от сервера
print(response.text)
