import argparse
import os

import tensorflow as tf
from loguru import logger

path_oldmodel = os.path.join("weights/bestmodel.h5")
path_diff = os.path.join("weights/param_diff.h5")

out_model = "weights/update_new_model.h5"

parser = argparse.ArgumentParser()
parser.add_argument(
    "-o",
    "--old_model",
    help="path to the old model",
    default=path_oldmodel,
)

parser.add_argument(
    "-d",
    "--diff_model",
    help="path to the diff model",
    default=path_diff,
)

parser.add_argument(
    "-n",
    "--new_model",
    help="path to the new model",
    default=out_model,
)
args = parser.parse_args()


def compute_param_diff_tflite(old_model_path, new_model_path):
    old_model = tf.lite.Interpreter(model_path=old_model_path)
    new_model = tf.lite.Interpreter(model_path=new_model_path)

    old_model.allocate_tensors()
    new_model.allocate_tensors()

    param_diff = {}
    for i in range(old_model.get_num_tensors()):
        old_param = old_model.get_tensor(i)
        new_param = new_model.get_tensor(i)

        diff = old_param - new_param
        param_diff[i] = diff

    return param_diff


def save_param_diff_tflite(param_diff, output_file_path):
    output_model = tf.lite.Interpreter(model_content=None)
    output_model.allocate_tensors()

    for i, diff in param_diff.items():
        output_model.set_tensor(i, diff)

    with open(output_file_path, "wb") as f:
        f.write(output_model.get_model())


if __name__ in "__main__":
    param_diff = compute_param_diff_tflite(args.old_model, args.new_model)
    save_param_diff_tflite(param_diff, args.diff_model)
    logger.debug("Create diff model")
