# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

import ctypes
import platform

# From Python 3.8 onwards, there is a reported bug in CDLL.__init__()
mode = dict(winmode=0) if platform.python_version() >= "3.8" else dict()

# Похоже здесь надо также поправить путь как в cant.py (не проверял):
# from pathlib import Path
# cant_folder = Path(__file__).parent
# lib = ctypes.CDLL(cant_folder.joinpath("capy.so"), **mode)
lib = ctypes.CDLL("./capy.so", **mode)


class cant(object):
    def __init__(self):
        # Declare input and output types for each method you intend to use
        lib.init.argtypes = []
        lib.init.restype = ctypes.c_void_p

        lib.InitId.argtypes = []
        lib.InitId.restype = None

        lib.ClearIdStructure.argtypes = []
        lib.ClearIdStructure.restype = None

        lib.AddIdStruct.argtypes = []
        lib.AddIdStruct.restype = None

        lib.getIdAttrSize.argtypes = []
        lib.getIdAttrSize.restype = ctypes.c_int

        lib.codeId.argtypes = []
        lib.codeId.restype = None

        lib.decodeId.argtypes = []
        lib.decodeId.restype = None

        lib.setAttrByNdx.argtypes = [ctypes.c_int, ctypes.c_uint8]
        lib.setAttrByNdx.restype = ctypes.c_bool

        lib.setAttrByName.argtypes = []
        lib.setAttrByName.restype = ctypes.c_bool

        lib.getAttrByNdx.argtypes = [ctypes.c_int]
        lib.getAttrByNdx.restype = ctypes.c_uint8

        lib.getAttrByName.argtypes = []
        lib.getAttrByName.restype = ctypes.c_uint8

        lib.getAdr.argtypes = []
        lib.getAdr.restype = ctypes.c_uint32

        lib.test.argtypes = []
        lib.test.restype = None

        self.obj = lib.init()

    def InitId(self):
        lib.InitId(self.obj)

    def ClearIdStructure(self):
        lib.ClearIdStructure(self.obj)

    def AddIdStruct(self, Name, Length):
        lib.AddIdStruct(self.obj, Name, Length)

    def getIdAttrSize(self):
        return lib.getIdAttrSize(self.obj)

    def codeId(self):
        lib.codeId(self.obj)

    def decodeId(self):
        lib.decodeId(self.obj)

    def setAttrByNdx(self, ndx, Val):
        return lib.setAttrByNdx(self.obj, ndx, Val)

    def setAttrByName(self, Name, Val):
        return lib.setAttrByName(self.obj, Name, Val)

    def getAttrByNdx(self, ndx):
        return lib.getAttrByNdx(self.obj, ndx)

    def getAttrByName(self, Name):
        return lib.getAttrByName(self.obj, Name)

    def getAdr(self):
        return lib.getAdr(self.obj)

    def test(self, s):
        lib.test(self.obj, s)


print("Start!")
CT = cant()
# print("test:")
# aa=b"321"
# ss=ctypes.c_char_p(b"CABB")
# CT.test(b"bbb")
# CT.test(aa)
# CT.test(ss)

print("getIdAttrSize=", CT.getIdAttrSize())
print("Step1 !")
print("setAttrByNdx=", CT.setAttrByNdx(1, 4))
print("setAttrByName=", CT.setAttrByName(b"Sender", 2))

print("getAttrByNdx=", CT.getAttrByNdx(3))
print("getAttrByName=", CT.getAttrByName(b"Sender"))
print("getAttrByName=", CT.getAttrByName(b"Attribute"))

print("Step2 !")
print("setAttrByNdx=", CT.setAttrByNdx(0, 1))
print("setAttrByNdx=", CT.setAttrByNdx(1, 2))
print("setAttrByNdx=", CT.setAttrByNdx(2, 3))
print("setAttrByNdx=", CT.setAttrByNdx(3, 4))

print("Step code !")
print("codeId()      ", CT.codeId())
print("decodeId()    ", CT.decodeId())
print("getAdr()    ", CT.getAdr())

print("Step3 ! Structure Test!")
CT.ClearIdStructure()
print("getIdAttrSize=", CT.getIdAttrSize())
CT.AddIdStruct("Test", 12)
CT.InitId()
print("getIdAttrSize=", CT.getIdAttrSize())


print("Finish!")
