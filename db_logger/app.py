# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

# https://docs.sqlalchemy.org/en/20/orm/quickstart.html
import os
from datetime import datetime

import can
from sqlalchemy import String, create_engine
from sqlalchemy.orm import DeclarativeBase, Mapped, Session, mapped_column

CAN_SERVER_HOST = os.getenv("CAN_SERVER_HOST")
bus = can.interface.Bus(
    f"ws://{CAN_SERVER_HOST}:54701/", bustype="remote", bitrate=500000
)

TABLE_NAME = "can_bus_message"


class Base(DeclarativeBase):
    pass


class CANBusMessage(Base):
    __tablename__ = TABLE_NAME
    id: Mapped[int] = mapped_column(primary_key=True)

    arbitration_id: Mapped[int]

    # FIXME длину посылки задавать в параметрах
    data: Mapped[str] = mapped_column(String(8))
    # Заменить тип строка на бинарные данные

    # Дата/время вставки в БД
    insert_date: Mapped[datetime] = mapped_column(default=datetime.utcnow)

    def __repr__(self) -> str:
        return (
            f"CANBusMessage(id={self.id!r}"
            f", arbitration_id={self.arbitration_id!r}"
            f", data={self.data!r}"
            f", insert_date={self.insert_date!r})"
        )


if __name__ == "__main__":
    print("DB logger application started")

    user = os.environ["DB_USER"]
    password = os.environ["DB_PASSWORD"]
    host = os.environ["DB_HOST"]
    port = os.environ["DB_PORT"]
    database = os.environ["DB_DATABASE"]

    engine_psql = create_engine(
        f"postgresql+psycopg2://{user}:{password}@{host}:{port}/{database}", echo=True
    )

    # https://docs.sqlalchemy.org/en/20/core/engines.html
    engine_lite = create_engine(f"sqlite:////sqlite_data/{TABLE_NAME}.db")

    # Создаём все таблицы во всех БД
    Base.metadata.create_all(engine_psql)
    Base.metadata.create_all(engine_lite)

    while True:
        with Session(engine_psql) as session_psql:
            # with Session(engine_lite) as session_lite:
            # TODO
            # Вынести работу с sqlite в отдельный микросервис. А класс CANBusMessage
            # вынести в библиотеку.
            # Альтернатива, если нужна работа сразу с двумя БД:
            # https://stackoverflow.com/questions/28238785/sqlalchemy-python-multiple-databases
            # https://docs.sqlalchemy.org/en/14/orm/persistence_techniques.html#session-partitioning

            msg = bus.recv()

            message = CANBusMessage(
                arbitration_id=msg.arbitration_id, data=msg.data.decode("ascii")
            )

            session_psql.add_all([message])
            session_psql.commit()
            # FIXME возможно здесь надо копить посылки, а не писать в БД по одной

            # При использовании sqlite раскомментировать строки ниже:
            # session_lite.add_all([message])
            # session_lite.commit()

            # Строки ниже нужны только для отладки
            # stmt = select(CANBusMessage).order_by(CANBusMessage.insert_date)
            # for message in session_lite.scalars(stmt):
            #     print(message)
            #     break  # выводим только одну запись и сразу выходим из цикла
